from node

env NODE_OPTIONS="--openssl-legacy-provider"

run mkdir /app
copy . /app
workdir /app

run yarn install
run yarn test
run yarn build

cmd yarn start

expose 3000
